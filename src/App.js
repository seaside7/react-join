import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Link } from "react-router-dom";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {email: '', password: '', isLogin: false};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePassChange = this.handlePassChange.bind(this);
  }
  

  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

  handlePassChange(event) {
    this.setState({password: event.target.value});
  }

  handleSubmit(event) {
    if(this.state.email === "support@joinprint.com.hk" && this.state.password === "support@joinprint.com.hk") {
      this.setState({isLogin: true});
      localStorage.setItem('isLogin', 'true');
      localStorage.setItem('email', this.state.email);
      event.preventDefault();
    }
    else {
      alert('Sorry Can\'t Login');
    }
    
  }

  handleLogout() {
    this.setState({isLogin: false});
    localStorage.setItem('isLogin', 'false');
  }

  render() {
    let home = null;
    let login = null;
    let checkedLogin = localStorage.getItem('isLogin') ? localStorage.getItem('isLogin') : this.state.isLogin;
    
    login = (
        <div className="auth-wrapper">
          <div className="auth-inner">
            <form onSubmit={this.handleSubmit}>
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" value={this.state.value} onChange={this.handleEmailChange}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" value={this.state.value} onChange={this.handlePassChange} />
                </div>

                <input type="submit" value="Submit" className="btn btn-primary btn-block"/>
                
            </form>
          </div>
          
        </div>
    );


    if (checkedLogin === 'true') {
      home = (
        <div className="auth-wrapper">
          <div className="auth-inner">
            <h3>Welcome {localStorage.getItem('email')}</h3>

            <button className="btn btn-primary btn-block"
            onClick={this.handleLogout }>Logout</button>
        </div>
      </div>
      );

      login = null;
    }
    return (
      <div className="App">
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
          <div className="container">
            <Link className="navbar-brand">Join Design</Link>
          </div>
        </nav>
        {login}
        {home}
        
      </div>
    );
  }
  }
  

export default App;
